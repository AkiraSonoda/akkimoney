CREATE TABLE `userinfo` (
  `avatarUUID` varchar(50) NOT NULL,
  `avatarName` varchar(128) NOT NULL,
  `simAddress` varchar(128) NOT NULL,
  `sessionId` varchar(50) DEFAULT NULL,
  `secureSessionId` varchar(50) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`avatarUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;