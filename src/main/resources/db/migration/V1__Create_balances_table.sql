CREATE TABLE `balances` (
  `avatarUUID` varchar(36) NOT NULL,
  `balance` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`avatarUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
