CREATE TABLE `transactions` (
  `UUID` varchar(36) NOT NULL,
  `sender` varchar(128) NOT NULL,
  `receiver` varchar(128) NOT NULL,
  `amount` int(10) NOT NULL,
  `objectUUID` varchar(36) DEFAULT NULL,
  `regionHandle` varchar(36) NOT NULL,
  `type` int(10) NOT NULL,
  `time` int(11) NOT NULL,
  `secure` varchar(36) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `commonName` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

