package org.akkisim.money.http

import akka.http.scaladsl.server.Directives._
import org.akkisim.money.http.routes._
import org.akkisim.money.services._

import scala.concurrent.ExecutionContext

class HttpService(balancesService: BalancesService,
                  transactionsService: TransactionsService,
                  usrinfoService: UserinfoService
                 )(implicit executionContext: ExecutionContext) /* with AuthServiceRoute */  extends CorsSupport {

    val pingRouter = new PingRoute()
    val userinfoRouter = new UserinfoServiceRoute(usrinfoService)
    val balancesRouter = new BalancesServiceRoute(balancesService)
    val transactionsRouter = new TransactionsServiceRoute(transactionsService)

    val routes =
        pathPrefix("rest") {
            corsHandler {
                userinfoRouter.route ~
                balancesRouter.route ~
                transactionsRouter.route ~
                pingRouter.route
                //    authRoute
            }

        }
}
