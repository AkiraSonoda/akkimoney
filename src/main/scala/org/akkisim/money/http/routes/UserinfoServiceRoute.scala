/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.http.routes

import akka.http.javadsl.server.RequestContext
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import org.akkisim.money.dataproviders.db.{UserinfoEntity, UserinfoEntityUpdate, UserinfoSession}
import org.akkisim.money.services.UserinfoService

import scala.concurrent.ExecutionContext

class UserinfoServiceRoute(val userinfoService: UserinfoService)(implicit executionContext: ExecutionContext) extends FailFastCirceSupport {

    import StatusCodes._
    import userinfoService._

    val route: Route = pathPrefix("userinfo") {
        pathEndOrSingleSlash {
            get {
                complete(getUserinfos.map(_.asJson))
            } ~
                post {
                    entity(as[UserinfoEntity]) {
                        userinfoCreate => {
                            if (!plausi(userinfoCreate)) {
                                complete (BadRequest)
                            } else {
                                complete (createUserInfo(userinfoCreate))
                            }
                        }
                    }
                }
        } ~
            get {
                path(Segment) { id =>
                    complete(getUserinfoByUser(id).map(_.asJson))
                }
            } ~
            put {
                path(Segment) { userid =>
                    entity(as[UserinfoEntityUpdate]) { userinfoUpdate =>
                        complete(updateUserinfo(userid, userinfoUpdate).map(_.asJson))
                    }
                }
            } ~
            delete {
                path(Segment) {
                    userid =>
                        onSuccess(deleteUserinfo(userid)) {
                            ignored => complete(NoContent)
                        }
                }
            } ~
            pathPrefix(Segment) { id =>
                pathEndOrSingleSlash {
                    post {
                        entity(as[UserinfoSession]) {
                            userinfoSession => {
                                headerValueByName("token") { token =>
                                    complete(updateSessionInfo(id, token, userinfoSession))
                                }
                            }
                        }
                    }
                } ~
                pathPrefix("token") {
                    post {
                        entity(as[UserinfoEntityUpdate]) {
                            userinfoEntityUpdate => {
                                    complete(getToken(userinfoEntityUpdate))
                            }
                        }
                    }

                }
            }


    }

    def plausi(userinfoEntity: UserinfoEntity): Boolean = {
        if (userinfoEntity.password == None && // No Security Element is wrong
            userinfoEntity.token == None &&
            userinfoEntity.secureSessionId == None &&
            userinfoEntity.sessionId == None) {
            return (false)
        } else if (userinfoEntity.password == None && // No Security Elements from inworld Login
            userinfoEntity.token == None &&
            userinfoEntity.secureSessionId != None &&
            userinfoEntity.sessionId != None) {
            return (true)
        } else if (userinfoEntity.password != None && // Security Elements from Web
            userinfoEntity.token != None &&
            userinfoEntity.secureSessionId == None &&
            userinfoEntity.sessionId == None) {
            return (true)
        } else {
            return (false)
        }

    }
}
