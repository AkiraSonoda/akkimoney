package org.akkisim.money.http.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import org.akkisim.money.dataproviders.db.{BalancesEntity, BalancesEntityUpdate}
import org.akkisim.money.services.{AuthenticationService, BalancesService}

import scala.concurrent.ExecutionContext

class BalancesServiceRoute(val balancesService: BalancesService)(implicit executionContext: ExecutionContext) extends FailFastCirceSupport  {

    import StatusCodes._
    import balancesService._

    val route: Route = pathPrefix("balances") {
        pathEndOrSingleSlash {
            get {
                headerValueByName("avatarUUID") {
                    avatarUUID => headerValueByName("token") {
                        token => complete(getBalances(avatarUUID,token).map(_.asJson))
                    }
                }
            } ~
            post {
                entity(as[BalancesEntity]) {
                    balanceCreate => {
                        headerValueByName("token") {
                            token => complete(createBalance(balanceCreate, token))
                        }
                    }
                }
            }
        } ~
        get {
            path(Segment) {
                id => {
                    headerValueByName("token") {
                        token => complete(getBalancesByUser(id, token).map(_.asJson))
                    }
                }
            }
        } ~
        put {
            path(Segment) { userid =>
                entity(as[BalancesEntityUpdate]) { balancesUpdate =>
                    headerValueByName("token") {
                        token => complete(updateBalance(userid, balancesUpdate, token).map(_.asJson))
                    }
                }
            }
        } ~
        delete {
            path(Segment) {
                userid =>
                    headerValueByName("token") {
                        token => onSuccess(deleteBalance(userid, token)) {
                        ignored => complete(NoContent)
                    }
                }
            }
        }
    }
}
