/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.http.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import scala.concurrent.duration._
import org.akkisim.money.dataproviders.db.{TransactionsEntity, TransactionsEntityUpdate}
import org.akkisim.money.services.{AuthenticationService, TransactionsService}

import scala.concurrent.ExecutionContext

class TransactionsServiceRoute(val transactionsService: TransactionsService)(implicit executionContext: ExecutionContext) extends FailFastCirceSupport {

    import StatusCodes._
    import transactionsService._

    val route: Route = pathPrefix("transactions") {
        pathEndOrSingleSlash {
            //            get {
            //                complete(getTransactions.map(_.asJson))
            //            } ~
            post {
                entity(as[TransactionsEntity]) { transactionsCreate =>
                    headerValueByName("token") {
                        token => complete(createTransaction(transactionsCreate, token))
                    }
                }
            }
        } ~
            pathPrefix("force") {
                pathEndOrSingleSlash {
                    post {
                        entity(as[TransactionsEntity]) {
                            transactionsCreate => complete(createTransaction(transactionsCreate))
                        }
                    }
                }
            }
        //        get {
        //            path(Segment) { uuid =>
        //                complete(getTransactionsByUUID(uuid).map(_.asJson))
        //            }
        //        } ~
        //        put {
        //            path(Segment) { uuid =>
        //                entity(as[TransactionsEntityUpdate]) { transactionsUpdate =>
        //                    complete(updateTransaction(uuid, transactionsUpdate).map(_.asJson))
        //                }
        //            }
        //        } ~
        //        delete {
        //            path(Segment) {
        //                uuid => onSuccess(deleteTransaction(uuid)) {
        //                    ignored => complete(NoContent)
        //                }
        //            }
        //        }
    }
}
