/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.http

import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive0, Route}
import com.typesafe.config.ConfigFactory

trait CorsSupport {
    lazy val allowedOriginHeader: `Access-Control-Allow-Origin` = {
        val config = ConfigFactory.load()
        val sAllowedOrigin = config.getString("cors.allowed-origin")
        if (sAllowedOrigin == "*")
            `Access-Control-Allow-Origin`.*
        else
            `Access-Control-Allow-Origin`(HttpOrigin(sAllowedOrigin))
    }

    private def addAccessControlHeaders()(): Directive0 = mapResponseHeaders { headers =>
        allowedOriginHeader +:
            `Access-Control-Allow-Credentials`(true) +:
            `Access-Control-Allow-Headers`("Token", "Content-Type", "X-Requested-With") +:
            headers
    }

    private def preflightRequestHandler: Route = options {
        complete(HttpResponse(200).withHeaders(
            `Access-Control-Allow-Methods`(OPTIONS, POST, PUT, GET, DELETE)
        )
        )
    }

    def corsHandler(r: Route): Route = addAccessControlHeaders()() {
        preflightRequestHandler ~ r
    }
}