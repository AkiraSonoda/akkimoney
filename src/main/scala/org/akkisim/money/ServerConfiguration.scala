/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money

import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.{Logger, LoggerFactory}

/**
  * @author Akira Sonoda
  */
object ServerConfiguration extends Configuration

trait Configuration {
    val logger: Logger = LoggerFactory.getLogger("ServerConfiguration")

    val config: Config = ConfigFactory.load
    config.checkValid(ConfigFactory.defaultReference)

    // Application Consts
    val httpHostName: String = getStringOrElse("http.hostname", "localhost")
    val httpInterface: String = getStringOrElse("http.interface", "0.0.0.0")
    val httpPort: Int = getIntOrElse("http.port", 9000)

    // Network Consts
    val networkCallTimeout: Int = getIntOrElse("http.timeout", 5) // Seconds

    // Database Consts
    val dbJDBCUrl: String = getStringOrElse("database.url", "localhost")
    val dbUsername: String = getStringOrElse("database.user", "dummy")
    val dbPassword: String = getStringOrElse("database.password", "dummy")


    def getIntOrElse(path: String, default: Int): Int = {
        if (config.hasPath(path)) {
            config.getInt(path)
        } else {
            logger.warn("Path %s - not found in config-file using default: %s".format(path, default))
            default
        }
    }


    def getStringOrElse(path: String, default: String): String = {
        if (config.hasPath(path)) {
            config.getString(path)
        } else {
            logger.warn("Path %s - not found in config-file using default: %s".format(path, default))
            default
        }
    }
}
