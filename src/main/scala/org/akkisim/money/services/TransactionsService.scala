/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.services

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, RequestEntity}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.util.ByteString
import org.akkisim.money.dataproviders.db._
import org.akkisim.money.dataproviders.helper.ProcessingResult
import org.akkisim.money.dataproviders.types.{TransactionStatus, TransactionType}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

/**
  * @author Akira Sonoda
  */

class TransactionsService(val databaseService: DatabaseService, userinfoService: UserinfoService, balancesService: BalancesService, authenticationService: AuthenticationService)
                         (implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer, executionContext: ExecutionContext)
    extends TransactionsEntityTable with BalancesEntityTable {

    import databaseService.driver.api._

    val log: Logger = LoggerFactory.getLogger("TransactionsService")

    var sndMessage: String = ""
    var rcvMessage: String = ""

    private val balanceMessageLandSale = "Paid the Money L$ %d for Land"
    private val balanceMessageRcvLandSale = ""
    private val balanceMessageSendGift = "Sent Gift L$ %d to %s"
    private val balanceMessageReceiveGift = "Received Gift L$ %d from %s"
    private val balanceMessagePayCharge = ""
    private val balanceMessageBuyObject = "Bought the Object L$ %d from %s"
    private val balanceMessageGetMoney = "Got the Money L$ %d from %s"
    private val balanceMessageBuyMoney = "Bought the Money L$ %s"
    private val balanceMessageReceiveMoney = "Received L$ %d from System"
    private val balanceMessageRollBack = "RollBack the Transaction: L$ %d from/to %s"

    def getTransactions: Future[Seq[TransactionsEntity]] = db.run(Transactions.result)

    def getTransactionsByUUID(uuid: String): Future[Option[TransactionsEntity]] = db.run(Transactions.filter(_.uuid === uuid).result.headOption)

    def createTransaction(transaction: TransactionsEntity, token: String): Future[ProcessingResult] = {
        authenticationService.authenticate(transaction.sender, token).flatMap {
            case true =>
                log.info("createTransaction")
                log.info("handleTransaction: Transfering money from " + transaction.sender + " to " + transaction.receiver)
                userinfoService.getUserinfoByUser(transaction.receiver).flatMap {
                    case Some(receivingUser) =>
                        log.debug("Receiving User found")
                        db.run(Transactions += transaction).flatMap {
                            case 1 =>
                                log.info("Transaction added")
                                if (transaction.amount != 0) {
                                    doTransfer(transaction.uuid)
                                } else {
                                    log.info("No messages for L$0 object")
                                    Future.successful(ProcessingResult(true))
                                }
                            case _ =>
                                log.info("Add Transaction failed")
                                Future.successful(ProcessingResult(false))
                        }
                    case None =>
                        log.info("Money Receiving User not found")
                        Future.successful(ProcessingResult(false))
                }
            case false =>
                Future.successful(ProcessingResult(false))
        }
    }

    def createTransaction(transaction: TransactionsEntity): Future[ProcessingResult] = {
        log.info("createForceTransaction")
        log.info("handleTransaction: Transfering money from " + transaction.sender + " to " + transaction.receiver)
        userinfoService.getUserinfoByUser(transaction.receiver).flatMap {
            case Some(receivingUser) =>
                log.debug("Receivering User found")
                db.run(Transactions += transaction).flatMap {
                    case 1 =>
                        log.info("Transaction added")
                        if (transaction.amount != 0) {
                            doTransfer(transaction.uuid)
                        } else {
                            log.info("No messages for L$0 object")
                            Future.successful(ProcessingResult(true))
                        }
                    case _ =>
                        log.info("Add Transaction failed")
                        Future.successful(ProcessingResult(false))
                }
            case None =>
                log.info("Money Receiving User not found")
                Future.successful(ProcessingResult(false))
        }
    }


    def deleteTransaction(uuid: String): Future[Int] = db.run(Transactions.filter(_.uuid === uuid).delete)

    def updateTransaction(uuid: String, transactionsUpdate: TransactionsEntityUpdate): Future[Option[TransactionsEntity]] = getTransactionsByUUID(uuid).flatMap {
        case Some(transaction) =>
            val updatedTransaction = transactionsUpdate.merge(transaction)
            db.run(Transactions.filter(_.uuid === uuid).update(updatedTransaction)).map(_ => Some(updatedTransaction))
        case None => Future.successful(None)
    }


    def doTransfer(transactionUUID: String): Future[ProcessingResult] = {
        log.debug("doTransfer(" + transactionUUID + ")")
        getTransactionsByUUID(transactionUUID).flatMap {
            // Reading the Transaction
            case Some(transaction) =>
                log.info("Transaction with UUID: " + transactionUUID + " Found")
                if (transaction.status == TransactionStatus.PendingStatus.asInstanceOf[Int]) {
                    // if (transaction.status == 1) {
                    log.info("Transaction Status is Pending")
                    balancesService.getBalancesByUser(transaction.sender).flatMap {
                        case Some(balance) =>
                            if (transaction.amount >= 0 && balance.balance >= transaction.amount) {
                                // Prepare Messages
                                if (transaction.`type` == TransactionType.Gift.asInstanceOf[Int]) {
                                    sndMessage = balanceMessageSendGift
                                    rcvMessage = balanceMessageReceiveGift
                                } else if (transaction.`type` == TransactionType.LandSale.asInstanceOf[Int]) {
                                    sndMessage = balanceMessageLandSale
                                    rcvMessage = balanceMessageRcvLandSale
                                } else if (transaction.`type` == TransactionType.PayObject.asInstanceOf[Int]) {
                                    sndMessage = balanceMessageBuyObject
                                } else if (transaction.`type` == TransactionType.ObjectPays.asInstanceOf[Int]) {
                                    // ObjectGiveMoney
                                    rcvMessage = balanceMessageGetMoney
                                }
                                // Withdraw Money
                                withdrawMoney(transaction.uuid, transaction.sender, transaction.amount).flatMap {
                                    case 2 =>
                                        balancesService.getBalancesByUser(transaction.receiver).flatMap {
                                            case Some(receiverBalance) =>
                                                giveMoney(transaction.uuid, transaction.receiver, transaction.amount).flatMap {
                                                    case 2 =>
                                                        log.info("Momey successfully passed to receiver")
                                                        notifyUsers(transaction.uuid, sndMessage, rcvMessage).flatMap {
                                                            case true => Future.successful(ProcessingResult(true))
                                                            case false => Future.successful(ProcessingResult(false))
                                                        }
                                                    case _ =>
                                                        log.error("Failed to pass the Money to the receiver")
                                                        log.info("Reverting the transaction")
                                                        giveMoney(transactionUUID, transaction.sender, transaction.amount).flatMap {
                                                            case 2 =>
                                                                log.info("Reverting the transaction successful")
                                                                Future.successful(ProcessingResult(false))
                                                            case _ =>
                                                                log.error("Reverting the transaction failed")
                                                                Future.successful(ProcessingResult(false))
                                                        }
                                                }
                                            case None =>
                                                balancesService.createBalance(BalancesEntity(
                                                    transaction.receiver, 0, TransactionStatus.SuccessStatus.asInstanceOf[Int])).flatMap {
                                                    case 1 =>
                                                        giveMoney(transaction.uuid, transaction.receiver, transaction.amount).flatMap {
                                                            case 2 =>
                                                                log.info("Momey successfully passed to receiver")
                                                                // Nofify Users
                                                                notifyUsers(transaction.uuid, sndMessage, rcvMessage).flatMap {
                                                                    case true => Future.successful(ProcessingResult(true))
                                                                    case false => Future.successful(ProcessingResult(false))
                                                                }
                                                            case _ =>
                                                                log.error("Failed to pass the Money to the receiver ")
                                                                log.info("Reverting the transaction")
                                                                giveMoney(transactionUUID, transaction.sender, transaction.amount).flatMap {
                                                                    case 2 =>
                                                                        log.info("Reverting the transaction successful")
                                                                        Future.successful(ProcessingResult(false))
                                                                    case _ =>
                                                                        log.error("Reverting the transaction failed")
                                                                        Future.successful(ProcessingResult(false))
                                                                }
                                                        }
                                                    case _ =>
                                                        log.error("Failed to create Balance for User: " + transaction.receiver)
                                                        log.info("Reverting the transaction")
                                                        giveMoney(transactionUUID, transaction.sender, transaction.amount).flatMap {
                                                            case 2 =>
                                                                log.info("Reverting the transaction successful")
                                                                Future.successful(ProcessingResult(false))
                                                            case _ =>
                                                                log.error("Reverting the transaction failed")
                                                                Future.successful(ProcessingResult(false))
                                                        }
                                                }
                                        }
                                    case _ =>
                                        log.error("Withdraw Money failed")
                                        Future.successful(ProcessingResult(false))
                                }
                            } else {
                                log.info("Not enough balance for user: " + transaction.sender + " to apply the transaction.")
                                Future.successful(ProcessingResult(false))
                            }
                        case None =>
                            log.info("Balance for Sender with UUID: " + transaction.sender + " Not Found")
                            Future.successful(ProcessingResult(false))
                    }
                } else {
                    log.info("Transaction with UUID: " + transactionUUID + " has expired")
                    Future.successful(ProcessingResult(false))
                }
            case None =>
                log.info("Transaction with UUID: " + transactionUUID + " Not Found")
                Future.successful(ProcessingResult(false))
        }
    }

    def withdrawMoney(transactionUUID: String, senderUUID: String, transactionAmount: Int): Future[Int] = {
        val transaction = (
            for {
                balance <- Balances.filter(_.user === senderUUID).map(p => p.balance).result.head
                balanceupdate = balance - transactionAmount
                balancesResult <- Balances.filter(_.user === senderUUID).map(p => p.balance).update(balanceupdate)
                transactionState <- Transactions.filter(_.uuid === transactionUUID).map(t => t.status).result.head
                result <- Transactions.filter(_.uuid === transactionUUID).map(t => t.status).update(TransactionStatus.PendingStatus.asInstanceOf[Int])
            } yield result + balancesResult
            ).transactionally
        db.run(transaction)
    }

    def giveMoney(transactionUUID: String, senderUUID: String, transactionAmount: Int): Future[Int] = {
        val transaction = (
            for {
                balance <- Balances.filter(_.user === senderUUID).map(p => p.balance).result.head
                balanceupdate = balance + transactionAmount
                balancesResult <- Balances.filter(_.user === senderUUID).map(p => p.balance).update(balanceupdate)
                transactionState <- Transactions.filter(_.uuid === transactionUUID).map(t => t.status).result.head
                result <- Transactions.filter(_.uuid === transactionUUID).map(t => t.status).update(TransactionStatus.SuccessStatus.asInstanceOf[Int])
            } yield result + balancesResult
            ).transactionally
        db.run(transaction)
    }

    def notifyUsers(transactionUUID: String, senderMessage: String, receiverMessage: String): Future[Boolean] = {
        log.info("NotifyTransfer: User has accepted the transaction, now continue with the transaction")

        getTransactionsByUUID(transactionUUID).flatMap {
            case Some(transaction) =>
                if (transaction.status == TransactionStatus.SuccessStatus.asInstanceOf[Int]) {
                    var updateSender = true
                    var updateReceiv = true
                    if (transaction.sender == transaction.receiver) {
                        updateSender = false
                    }
                    //if (transaction.Type==(int)TransactionType.UploadCharge) return true;
                    if (transaction.`type` == TransactionType.UploadCharge.asInstanceOf[Int]) {
                        updateReceiv = false
                    }
                    notifyOnlineAvatar(updateSender, transaction.receiver, senderMessage, transaction.amount).flatMap {
                        _ =>
                            notifyOnlineAvatar(updateReceiv, transaction.sender, receiverMessage, transaction.amount).flatMap {
                                _ =>
                                    notifyUsers(transaction).flatMap {
                                        case Some(result) => if (result) {
                                            Future.successful(true)
                                        } else {
                                            Future.successful(false)
                                        }
                                        case None =>
                                            Future.successful(true)
                                    }
                            }
                    }
                } else {
                    Future.successful(false)
                }
            case None =>
                log.error("NotifyTransfer: Transaction " + transactionUUID + " failed")
                Future.successful(false)
        }
    }

    def notifyOnlineAvatar(updateAvatar: Boolean, avatarUUID: String, msg2Sender: String, amount: Int): Future[Boolean] = {
        log.debug("Notify online Avatar: " + avatarUUID)
        if (updateAvatar) userinfoService.getUserinfoByUser(avatarUUID).flatMap {
            case Some(userinfo) =>
                val snd_message = msg2Sender.format(amount, userinfo.avatarName)
                updateBalance(userinfo, snd_message).flatMap {
                    _: Boolean =>
                        // We actually don't care about the outcome of the update Request
                        Future.successful(true)
                }
            case None =>
                log.error("No Balance for User %s".format(avatarUUID))
                Future.successful(true)

        } else {
            Future.successful(true)
        }
    }

    def updateBalance(userinfo: UserinfoEntity, message: String): Future[Boolean] = {
        log.debug("updateBalance of:" + userinfo.avatarName)
        balancesService.getBalancesByUser(userinfo.avatarUUID).flatMap {
            case Some(balance) =>
                val xmlRequest = fillUserUpdateXML(userinfo, balance.balance, message)
                // Do the actual call to OpenSim
                serverRequest(xmlRequest, userinfo.simAddress).flatMap {
                    _ => Future.successful(true)
                }
            case None =>
                log.info("Could not notify receiving notifyOnlineAvatar: %s".format(userinfo.avatarName))
                Future.successful(false)

        }


    }

    def notifyUsers(transaction: TransactionsEntity): Future[Option[Boolean]] = {
        log.debug("Notify Sender about Transaction")
        if (transaction.`type` == TransactionType.PayObject.asInstanceOf[Int]) {
            log.info("Now notify opensim to give object to customer %s ".format(transaction.sender))

            userinfoService.getUserinfoByUser(transaction.sender).flatMap {
                case Some(userinfo) =>
                    val xmlRequest = fillTransferMoneyXML(userinfo, transaction)
                    serverRequest(xmlRequest, userinfo.simAddress).flatMap {
                        case true => Future.successful(Some(true))
                        case false =>
                            // TODO Do the Rollback
                            Future.successful(Some(false))
                    }
                case None =>
                    log.error("unable to find the User to give the Object to")
                    Future.successful(Some(false))
            }
        } else {
            Future.successful(None)
        }
    }

    //            if (user != null) {
    //                val responseTable = genericCurrencyXMLRPCRequest(requestTable, "OnMoneyTransfered", user.SimIP)
    //                if (responseTable != null && responseTable.ContainsKey("success")) { //User not online or failed to get object ?
    //                    if (!responseTable("success").asInstanceOf[Nothing]) {
    //                        m_log.ErrorFormat("[MONEY RPC]: NotifyTransfer: User {0} can't get the object, rolling back", transaction.Sender)
    //                        if (RollBackTransaction(transaction)) m_log.ErrorFormat("[MONEY RPC]: NotifyTransfer: Transaction {0} failed but roll back succeeded", transactionUUID.ToString)
    //                        else m_log.ErrorFormat("[MONEY RPC]: NotifyTransfer: Transaction {0} failed and roll back failed as well", transactionUUID.ToString)
    //                    }
    //                    else {
    //                        m_log.InfoFormat("[MONEY RPC]: NotifyTransfer: Transaction {0} finished successfully", transactionUUID.ToString)
    //                        return true
    //                    }
    //                }
    //            }
    //
    //            return false

    def fillTransferMoneyXML(userinfo: UserinfoEntity, transaction: TransactionsEntity): String = {
        val xmlOnMoneyTransfer = "<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>OnMoneyTransfered</methodName>" +
            "<params><param><value><struct><member>" +
            "<name>objectID</name><value><string>%s</string></value></member><member>" + // 1. Param ObjectID
            "<name>transactionType</name><value><i4>%d</i4></value></member><member>" + // 2. Param TransactionType
            "<name>clientSessionID</name><value><string>%s</string></value></member><member>" + // 3. Param clientSessionId
            "<name>clientSecureSessionID</name><value><string>%s</string></value></member><member>" + // 4. Param clientSecureSessionId
            "<name>clientUUID</name><value><string>%s</string></value></member><member>" + // 5. Param clientUUID
            "<name>amount</name><value><i4>%d</i4></value></member><member>" + // 6. Param amount
            "<name>regionHandle</name><value><string>%s</string></value></member><member>" + // 7. Param regionHandle
            "<name>receiverUUID</name><value><string>%s</string></value></member>" + // 8. Param receiverUUID
            "</struct></value></param></params></methodCall>"


        var objectUUID = ""
        transaction.objectuuid match {
            case Some(uuid) => objectUUID = uuid
            case None => objectUUID = ""
        }

        var senderSessionId = ""
        userinfo.sessionId match {
            case Some(sessionId) => senderSessionId = sessionId
            case None => senderSessionId = "00000000-0000-0000-0000-000000000000"
        }

        var senderSecureSessionId = ""
        userinfo.secureSessionId match {
            case Some(secureSessionId) => senderSecureSessionId = secureSessionId
            case None => senderSecureSessionId = "00000000-0000-0000-0000-000000000000"
        }

        val xmlRequest = xmlOnMoneyTransfer.format(
            objectUUID, // 1. param
            transaction.`type`, // 2. param
            senderSessionId, // 3. param
            senderSecureSessionId, // 4. Param
            userinfo.avatarUUID, // 5. Param
            transaction.amount, // 6. param
            transaction.regionhandle, // 7. param
            transaction.receiver // 8. param
        )

        xmlRequest
    }

    def fillUserUpdateXML(userinfo: UserinfoEntity, balance: Int, message: String): String = {
        val xmlUpdateBalance = "<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>UpdateBalance</methodName><params><param><value><struct><member>" +
            "<name>clientSessionID</name><value><string>%s</string></value></member><member>" + // 1. param clientSessionId
            "<name>clientSecureSessionID</name><value><string>%s</string></value></member><member>" + // 2. param clientSecureSessionId
            "<name>Message</name><value><string>%s</string></value></member><member>" + // 3. param message
            "<name>Balance</name><value><i4>%d</i4></value></member><member>" + // 4. param balance
            "<name>clientUUID</name><value><string>%s</string></value></member>" + // 5. param clientUUID
            "</struct></value></param></params></methodCall>"


        var senderSessionId = ""
        userinfo.sessionId match {
            case Some(sessionId) => senderSessionId = sessionId
            case None => senderSessionId = "00000000-0000-0000-0000-000000000000"
        }

        var senderSecureSessionId = ""
        userinfo.secureSessionId match {
            case Some(secureSessionId) => senderSecureSessionId = secureSessionId
            case None => senderSecureSessionId = "00000000-0000-0000-0000-000000000000"
        }

        val xmlRequest = xmlUpdateBalance.format(
            senderSessionId, // 1. param
            senderSecureSessionId, // 2. param
            message, // 3. param
            balance, // 4. param
            userinfo.avatarUUID // 5. param
        )

        xmlRequest
    }

    def serverRequest(xmlBody: String, uri: String): Future[Boolean] = {

        val responseEntity = for {
            request <- Marshal(xmlBody).to[RequestEntity]
            response <- Http().singleRequest(HttpRequest(method = HttpMethods.POST, uri = uri, entity = request))
            entity <- Unmarshal(response.entity).to[ByteString]
        } yield entity

        responseEntity.flatMap {
            response =>
                if (response.utf8String.contains("success")) {
                    Future.successful(true)
                } else {
                    Future.successful(false)
                }
        }
    }

    def rollBackTransaction(transaction: TransactionsEntity): Future[Boolean] = {
        log.debug("RollBackTransaction:")

        withdrawMoney(transaction.uuid, transaction.receiver, transaction.amount).flatMap {
            case 2 =>
                giveMoney(transaction.uuid, transaction.sender, transaction.amount).flatMap {
                    case 2 =>
                        log.info("RollBackTransaction: Transaction %s successfully".format(transaction.uuid))
                        updateTransactionStatus(transaction.uuid, TransactionStatus.FailedStatus.asInstanceOf[Int], "The buyer failed to get the object, roll back the transaction").flatMap {
                            _: Int =>
                                notifyRollback(transaction).flatMap {
                                    case true => Future.successful(true)
                                    case false => Future.successful(false)
                                }
                        }
                    case _ =>
                        Future.successful(false)

                }
            case _ =>
                Future.successful(false)
        }

        Future.successful(true)
    }

    def updateTransactionStatus(transactionUUID: String, transactionStatus: Int, message: String): Future[Int] = {
        db.run(Transactions.filter(_.uuid === transactionUUID)
            .map(x => (x.status, x.description))
            .update(transactionStatus, Some(message))
        )
    }


    def notifyRollback(transaction: TransactionsEntity): Future[Boolean] = {
        var senderName = "unknown user"
        var receiverName = "unknown user"

        var updateSender = true
        var updateReceiv = true

        if (transaction.sender == transaction.receiver) {
            updateSender = false
        }

        notifyOnlineAvatar(updateSender, transaction.receiver, balanceMessageRollBack, transaction.amount).flatMap {
            _ =>
                notifyOnlineAvatar(updateReceiv, transaction.sender, balanceMessageRollBack, transaction.amount).flatMap {
                    _ => Future.successful(true)
                }
        }
    }
}