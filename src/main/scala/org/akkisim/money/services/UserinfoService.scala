/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.services
import java.util.UUID

import org.akkisim.money.dataproviders.db.{UserinfoSession, _}
import org.akkisim.money.dataproviders.types.UUID_ZERO
import org.akkisim.money.dataproviders.helper.Token
import org.slf4j.{Logger, LoggerFactory}


import scala.concurrent.{ExecutionContext, Future}

/**
  * @author Akira Sonoda
  */

class UserinfoService (val databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends UserinfoEntityTable {
    val log: Logger = LoggerFactory.getLogger("UserInfoService")

    import databaseService.driver.api._

    def createUserInfo(userinfo: UserinfoEntity): Future[Int] = {
        log.debug("Creating User: " + userinfo.avatarName)
        db.run(Userinfo += userinfo)
    }

    def deleteUserinfo(user: String): Future[Int] = db.run(Userinfo.filter(_.avatarUUID === user).delete)

    def getUserinfos: Future[Seq[UserinfoEntity]] = {
        log.debug("Getting UserInfo of all Users")
        db.run(Userinfo.result)
    }

    def getUserinfoByUser(userid: String): Future[Option[UserinfoEntity]] = {
        log.debug("Getting UserInfo for User: " + userid)
        db.run(Userinfo.filter(_.avatarUUID === userid).result.headOption)
    }

    def updateUserinfo(user: String, userinfoUpdate: UserinfoEntityUpdate): Future[Option[UserinfoEntity]] = getUserinfoByUser(user).flatMap {
        case Some(userinfo) =>
            log.debug("UserInfo for User "+user+" found")
            val updatedUserinfo = userinfoUpdate.merge(userinfo)
            db.run(Userinfo.filter(_.avatarUUID === user).update(updatedUserinfo)).map(_ => Some(updatedUserinfo))
        case None =>
            log.debug("UserInfo for User "+user+" not found")
            Future.successful(None)
    }

    def updateSessionInfo(userId: String, token: String, userinfoSession: UserinfoSession): Future[ModifiedRows] = getUserinfoByUser(userId).flatMap {
        case Some(userinfo) =>
            userinfo.token match {
                case Some(utoken) =>
                    if(utoken.equals(token)) {
                        val updatedUserInfo = UserinfoEntity(
                            userinfo.avatarUUID,
                            userinfo.avatarName,
                            userinfo.simAddress,
                            None,
                            None,
                            None,
                            None
                        )
                        db.run(Userinfo.filter(_.avatarUUID === userId).update(updatedUserInfo)).map(i => ModifiedRows(i))
                    } else {
                        Future.successful( ModifiedRows(0) )
                    }
                case None =>  Future.successful( ModifiedRows(0) )
            }
        case None => Future.successful( ModifiedRows(0) )
    }

    def getToken(userinfoUpdate: UserinfoEntityUpdate): Future[Token] = getUserinfoByUser(userinfoUpdate.avatarUUID).flatMap {
        case Some(userinfo) =>
            val updatedUserinfo = userinfoUpdate.merge(userinfo)
            db.run(Userinfo.filter(_.avatarUUID === userinfoUpdate.avatarUUID).update(updatedUserinfo)).map(_ => updatedUserinfo.token match {
                    case Some(token) =>  Token(token)
                    case None =>
                        log.error("User Not Found: " + userinfoUpdate.avatarName +" - uuid: " +updatedUserinfo.avatarUUID +" returning empty UUID")
                        Token(UUID_ZERO)

                }
            )
        case None =>
            val userInf = UserinfoEntity(
                avatarUUID = userinfoUpdate.avatarUUID,
                avatarName = userinfoUpdate.avatarName,
                simAddress = userinfoUpdate.simAddress,
                sessionId = userinfoUpdate.sessionId,
                secureSessionId = userinfoUpdate.secureSessionId,
                password = userinfoUpdate.password,
                token = Some(UUID.randomUUID().toString))
            db.run(Userinfo += userInf).map(_=> userInf.token match {
                case Some(token) => Token(token)
                case None =>
                    log.error("User Not Found: " + userinfoUpdate.avatarName +" - uuid: " +userinfoUpdate.avatarUUID +" returning empty UUID")
                    Token(UUID_ZERO)
            })
    }

}