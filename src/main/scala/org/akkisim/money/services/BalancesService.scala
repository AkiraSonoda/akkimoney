/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.services


import org.akkisim.money.dataproviders.db.{BalancesEntity, BalancesEntityTable, BalancesEntityUpdate}
import org.akkisim.money.dataproviders.types.AvatarUUID
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

/**
  * @author Akira Sonoda
  */
class BalancesService(val databaseService: DatabaseService, authenticationService: AuthenticationService)(implicit executionContext: ExecutionContext) extends BalancesEntityTable {
    val log: Logger = LoggerFactory.getLogger("BalancesService")

    import databaseService.driver.api._

    def getBalances(avatarUUID: AvatarUUID, token: String): Future[Seq[BalancesEntity]] =
        authenticationService.authenticate(avatarUUID,token).flatMap {
            case true => db.run(Balances.result)
            case false => Future.successful(Seq[BalancesEntity]())
        }

    def getBalancesByUser(avatarUUID: String): Future[Option[BalancesEntity]] = db.run(Balances.filter(_.user === avatarUUID).result.headOption)

    def getBalancesByUser(avatarUUID: String, token: String): Future[Option[BalancesEntity]] =
        authenticationService.authenticate(avatarUUID, token).flatMap {
            case true => db.run(Balances.filter(_.user === avatarUUID).result.headOption)
            case false => Future.successful(None)
        }

    // For Testing purposes only
    def createBalance(balance: BalancesEntity): Future[Int] = {
        db.run(Balances += balance)
    }


    def createBalance(balance: BalancesEntity, token: String): Future[Int] =
        authenticationService.authenticate(balance.avatarUUID, token).flatMap {
            case true => db.run(Balances += balance)
            case false => Future.successful(0)
        }


    def deleteBalance(avatarUUID: String, token: String): Future[Int] = {
        authenticationService.authenticate(avatarUUID,token).flatMap {
            case true => db.run(Balances.filter(_.user === avatarUUID).delete)
            case false => Future.successful(0)
        }
    }

    def updateBalance(avatarUUID: String, balanceUpdate: BalancesEntityUpdate, token: String): Future[Option[BalancesEntity]] =
        authenticationService.authenticate(avatarUUID, token).flatMap {
            case true =>
                getBalancesByUser(avatarUUID).flatMap {
                    case Some(balance) =>
                        val updatedBalance = balanceUpdate.merge(balance)
                        db.run(Balances.filter(_.user === avatarUUID).update(updatedBalance)).map(_ => Some(updatedBalance))
                    case None => Future.successful(None)
                }
            case false => Future.successful(None)
        }

    def createUpdateBalance(avatarUUID: String, token: String, balanceUpdate: BalancesEntityUpdate): Future[Option[BalancesEntity]] =
        authenticationService.authenticate(avatarUUID, token).flatMap {
            case true =>
                getBalancesByUser(avatarUUID).flatMap {
                    case Some(balance) =>
                        val updatedBalance = balanceUpdate.merge(balance)
                        db.run(Balances.filter(_.user === avatarUUID).update(updatedBalance)).map(_ => Some(updatedBalance))
                    case None =>
                        val balanceEntity = new BalancesEntity(
                            avatarUUID = balanceUpdate.avatarUUID,
                            balance = balanceUpdate.balance,
                            status = balanceUpdate.status)
                        db.run(Balances += balanceEntity).map(_ => Some(balanceEntity))
                }
            case false => Future.successful(None)
        }
}