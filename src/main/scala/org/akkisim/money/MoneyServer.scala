/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import akka.util.Timeout
import org.akkisim.money.http.HttpService
import org.akkisim.money.services._
import org.slf4j.LoggerFactory

import scala.concurrent.duration._
import scala.language.postfixOps


/**
  * @author Akira Sonoda
  */
object MoneyServer extends App with Configuration with Directives {
    val log = LoggerFactory.getLogger("MoneyServer")
    log.info("Starting App")

    // we need an ActorSystem to host our application in
    implicit val system = ActorSystem("moneyserver")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher
    implicit val timeout = Timeout(networkCallTimeout seconds)

    val flywayService = new FlywayService(dbJDBCUrl, dbUsername, dbPassword)
    val migrationResult = flywayService.migrateDatabaseSchema()
    log.info("Result of DatabaseMigration: %d".format(migrationResult))

    val databaseService = new DatabaseService()
    val userinfoService = new UserinfoService(databaseService)
    val authenticationService = new AuthenticationService(databaseService, userinfoService)
    val balancesService = new BalancesService(databaseService, authenticationService)
    val transactionsService = new TransactionsService(databaseService,userinfoService,balancesService, authenticationService)

    val httpService = new HttpService(balancesService, transactionsService, userinfoService)
    Http().bindAndHandle(httpService.routes, httpInterface, httpPort)

}
