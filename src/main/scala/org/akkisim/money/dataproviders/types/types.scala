/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.dataproviders

/**
  * @author Akira Sonoda
  */
package object types {
    type AvatarUUID = String
    val UUID_ZERO = "00000000-0000-0000-0000-000000000000"

    object TransactionType extends Enumeration {
        type TransactionType = Int
        // One-Time Charges
        val GroupCreate = 1002
        val GroupJoin = 1004
        val UploadCharge = 1101
        val LandAuction = 1102
        val ClassifiedCharge = 1103
        // Recurrent Charges
        val ParcelDirFee = 2003
        val ClassifiedRenew = 2005
        val ScheduledFee = 2900
        // Inventory Transactions
        val GiveInventory = 3000
        // Transfers Between Users
        val ObjectSale = 5000
        val Gift = 5001
        val LandSale = 5002
        val ReferBonus = 5003
        val InvntorySale = 5004
        val RefundPurchase = 5005
        val LandPassSale = 5006
        val DwellBonus = 5007
        val PayObject = 5008
        val ObjectPays = 5009
        val BuyMoney = 5010
        val MoveMoney = 5011
        // Group Transactions
        val GroupLiability = 6003
        val GroupDividend = 6004
        // Stipend Credits
        val StipendPayment = 10000

    }

    object TransactionStatus extends Enumeration {
        type TransactionStatus = Int
        val SuccessStatus = 0
        val PendingStatus = 1
        val FailedStatus = 2
    }

}
