/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.dataproviders.db

import org.akkisim.money.services.DatabaseService
import slick.lifted.{MappedProjection, ProvenShape}

/**
  * @author Akira Sonoda
  */
trait BalancesEntityTable extends DatabaseService {
    import driver.api._

    /** GetResult implicit for fetching BalancesRow objects using plain SQL queries */
    // implicit def GetResultBalancesRow(implicit e0: GR[String], e1: GR[Int], e2: GR[Option[Byte]]): GR[BalancesEntity] = GR{
    //     prs => import prs._
    //         BalancesEntity.tupled((<<[String], <<[Int], <<?[Byte]))
    // }

    /** Table description of table balances. Objects of this class serve as prototypes for rows in queries. */
    class Balances(tag: Tag) extends Table[BalancesEntity](tag, "balances")  {
        def * : ProvenShape[BalancesEntity] = (user, balance, status) <> (BalancesEntity.tupled, BalancesEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? : MappedProjection[Option[BalancesEntity], (Option[String], Option[Int], Option[Int])] = (Rep.Some(user), Rep.Some(balance), Rep.Some(status))<>({ r=>import r._; _1.map(_=> BalancesEntity.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column avatarUUID SqlType(VARCHAR), PrimaryKey, Length(128,true) */
        val user: Rep[String] = column[String]("avatarUUID", O.PrimaryKey, O.Length(128,varying=true))
        /** Database column balance SqlType(INT) */
        val balance: Rep[Int] = column[Int]("balance")
        /** Database column status SqlType(TINYINT), Default(None) */
        val status: Rep[Int] = column[Int]("status", O.Default(0))
    }
    /** Collection-like TableQuery object for table Balances */
    lazy val Balances = new TableQuery(tag => new Balances(tag))

}
