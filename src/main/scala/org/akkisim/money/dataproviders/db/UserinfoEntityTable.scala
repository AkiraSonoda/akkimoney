/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.dataproviders.db

import org.akkisim.money.services.DatabaseService
import slick.lifted.{MappedProjection, ProvenShape}

/**
  * @author Akira Sonoda
  */
trait UserinfoEntityTable extends DatabaseService {
    import driver.api._

    /** GetResult implicit for fetching UserinfoRow objects using plain SQL queries */
    // implicit def GetResultUserinfoRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[UserinfoEntity] = GR{
    //    prs => import prs._
    //        UserinfoEntity.tupled((<<[String], <<[String], <<[String], <<?[String], <<?[String]))
    // }
    /** Table description of table userinfo. Objects of this class serve as prototypes for rows in queries. */
    class Userinfo(tag: Tag) extends Table[UserinfoEntity](tag, "userinfo") {
        def * : ProvenShape[UserinfoEntity] = (avatarUUID, avatarName, simAddress, sessionId, secureSessionId, password, token) <> (UserinfoEntity.tupled, UserinfoEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? : MappedProjection[Option[UserinfoEntity], (Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String])] =
            (Rep.Some(avatarUUID), Rep.Some(avatarName), Rep.Some(simAddress), sessionId, secureSessionId, password, token).shaped.<>({ r=>import r._; _1.map(_=> UserinfoEntity.tupled((_1.get, _2.get, _3.get, _4, _5, _6, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        val avatarUUID: Rep[String] = column[String]("avatarUUID", O.PrimaryKey, O.Length(50,varying=true))
        val avatarName: Rep[String] = column[String]("avatarName", O.Length(128,varying=true))
        val simAddress: Rep[String] = column[String]("simAddress", O.Length(128,varying=true))
        val sessionId: Rep[Option[String]] = column[Option[String]]("sessionId", O.Length(50,varying=true), O.Default(None))
        val secureSessionId: Rep[Option[String]] = column[Option[String]]("secureSessionId", O.Length(50,varying=true), O.Default(None))
        val password: Rep[Option[String]] = column[Option[String]]("password", O.Length(64,varying=true), O.Default(None))
        val token: Rep[Option[String]] = column[Option[String]]("token", O.Length(50,varying=true), O.Default(None))
    }
    /** Collection-like TableQuery object for table Userinfo */
    lazy val Userinfo = new TableQuery(tag => new Userinfo(tag))

}
