/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.dataproviders.db

import java.util.UUID

/** Entity class storing rows of table Userinfo
  *
  * @param avatarUUID Database column avatarUUID SqlType(VARCHAR), PrimaryKey, Length(50,true)
  * @param avatarName Database column avatarName SqlType(VARCHAR), Length(128,true)
  * @param simAddress Database column simAddress SqlType(VARCHAR), Length(128,true)
  * @param sessionId Database column sessionId  SqlType(VARCHAR), Length(50,true), Default(none)
  * @param secureSessionId Database column secureSessionId SqlType(VARCHAR), Length(50,true), Default(none)
  * @param password Database column password SqlType(VARCHAR), Length(64,true), Default(none)
  * @param token  Database column token SqlType(VARCHAR), Length(64,true), Default(None)
  * @author Akira Sonoda
  */
final case class UserinfoEntity(avatarUUID: String,
                                avatarName: String,
                                simAddress: String,
                                sessionId: Option[String] = None,
                                secureSessionId: Option[String] = None,
                                password: Option[String] = None,
                                token: Option[String] = None
                               )


final case class UserinfoEntityUpdate(avatarUUID: String,
                                      avatarName: String,
                                      simAddress: String,
                                      sessionId: Option[String] = None,
                                      secureSessionId: Option[String] = None,
                                      password: Option[String] = None,
                                      token: Option[String] = None
                                     ) {
    def merge(userinfo: UserinfoEntity): UserinfoEntity = UserinfoEntity(avatarUUID,
        avatarName,
        simAddress,
        sessionId,
        secureSessionId,
        password,
        Some(UUID.randomUUID().toString))
}

final case class UserinfoSession(avatarUUID: String,
                                 sessionId: Option[String] = None,
                                 secureSessionId: Option[String] = None
                                )

