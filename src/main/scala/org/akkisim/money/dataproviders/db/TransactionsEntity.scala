/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.dataproviders.db

/** Entity class storing rows of table Transactions
  *
  * @param uuid         Database column UUID SqlType(VARCHAR), PrimaryKey, Length(36,true)
  * @param sender       Database column sender SqlType(VARCHAR), Length(128,true)
  * @param receiver     Database column receiver SqlType(VARCHAR), Length(128,true)
  * @param amount       Database column amount SqlType(INT)
  * @param objectuuid   Database column objectUUID SqlType(VARCHAR), Length(36,true), Default(None)
  * @param regionhandle Database column regionHandle SqlType(VARCHAR), Length(36,true)
  * @param `type`       Database column type SqlType(INT)
  * @param time         Database column time SqlType(INT)
  * @param secure       Database column secure SqlType(VARCHAR), Length(36,true)
  * @param status       Database column status SqlType(BIT)
  * @param commonname   Database column commonName SqlType(VARCHAR), Length(128,true), Default(None)
  * @param description  Database column description SqlType(VARCHAR), Length(255,true), Default(None)
  *
  * @author Akira Sonoda
  */
final case class TransactionsEntity(uuid: String,
                                    sender: String,
                                    receiver: String,
                                    amount: Int,
                                    objectuuid: Option[String] = None,
                                    regionhandle: String,
                                    `type`: Int,
                                    time: Int,
                                    secure: String,
                                    status: Int,
                                    commonname: Option[String] = None,
                                    description: Option[String] = None)


final case class TransactionsEntityUpdate(uuid: String,
                                    sender: String,
                                    receiver: String,
                                    amount: Int,
                                    objectuuid: Option[String] = None,
                                    regionhandle: String,
                                    `type`: Int,
                                    time: Int,
                                    secure: String,
                                    status: Int,
                                    commonname: Option[String] = None,
                                    description: Option[String] = None) {
    def merge(transactions: TransactionsEntity): TransactionsEntity = {
        TransactionsEntity(uuid,sender,receiver,amount,objectuuid,regionhandle, `type`, time,secure,status,commonname,description)
    }
}
