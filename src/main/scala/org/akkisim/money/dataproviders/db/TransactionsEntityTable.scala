/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.dataproviders.db

import org.akkisim.money.services.DatabaseService
import slick.lifted.{MappedProjection, ProvenShape}

/**
  * @author Akira Sonoda
  */
trait TransactionsEntityTable extends DatabaseService {
    import driver.api._

    /** GetResult implicit for fetching TransactionsRow objects using plain SQL queries */
    // implicit def GetResultTransactionsRow(implicit e0: GR[String], e1: GR[Int], e2: GR[Option[String]], e3: GR[Boolean]): GR[TransactionsEntity] = GR{
    //    prs => import prs._
    //        TransactionsEntity.tupled((<<[String], <<[String], <<[String], <<[Int], <<?[String], <<[String], <<[Int], <<[Int], <<[String], <<[Boolean], <<?[String], <<?[String]))
    // }
    /** Table description of table transactions. Objects of this class serve as prototypes for rows in queries.
      *  NOTE: The following names collided with Scala keywords and were escaped: type */
    class Transactions(tag: Tag) extends Table[TransactionsEntity](tag, "transactions") {
        def * : ProvenShape[TransactionsEntity] = (uuid, sender, receiver, amount, objectuuid, regionhandle, `type`, time, secure, status, commonname, description) <> (TransactionsEntity.tupled, TransactionsEntity.unapply)
        /** Maps whole row to an option. Useful for outer joins. */
        def ? : MappedProjection[Option[TransactionsEntity], (Option[String], Option[String], Option[String], Option[Int], Option[String], Option[String], Option[Int], Option[Int], Option[String], Option[Int], Option[String], Option[String])] = (Rep.Some(uuid), Rep.Some(sender), Rep.Some(receiver), Rep.Some(amount), objectuuid, Rep.Some(regionhandle), Rep.Some(`type`), Rep.Some(time), Rep.Some(secure), Rep.Some(status), commonname, description).shaped.<>({ r=>import r._; _1.map(_=> TransactionsEntity.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6.get, _7.get, _8.get, _9.get, _10.get, _11, _12)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

        /** Database column UUID SqlType(VARCHAR), PrimaryKey, Length(36,true) */
        val uuid: Rep[String] = column[String]("UUID", O.PrimaryKey, O.Length(36,varying=true))
        /** Database column sender SqlType(VARCHAR), Length(128,true) */
        val sender: Rep[String] = column[String]("sender", O.Length(128,varying=true))
        /** Database column receiver SqlType(VARCHAR), Length(128,true) */
        val receiver: Rep[String] = column[String]("receiver", O.Length(128,varying=true))
        /** Database column amount SqlType(INT) */
        val amount: Rep[Int] = column[Int]("amount")
        /** Database column objectUUID SqlType(VARCHAR), Length(36,true), Default(None) */
        val objectuuid: Rep[Option[String]] = column[Option[String]]("objectUUID", O.Length(36,varying=true), O.Default(None))
        /** Database column regionHandle SqlType(VARCHAR), Length(36,true) */
        val regionhandle: Rep[String] = column[String]("regionHandle", O.Length(36,varying=true))
        /** Database column type SqlType(INT)
          *  NOTE: The name was escaped because it collided with a Scala keyword. */
        val `type`: Rep[Int] = column[Int]("type")
        /** Database column time SqlType(INT) */
        val time: Rep[Int] = column[Int]("time")
        /** Database column secure SqlType(VARCHAR), Length(36,true) */
        val secure: Rep[String] = column[String]("secure", O.Length(36,varying=true))
        /** Database column status SqlType(BIT) */
        val status: Rep[Int] = column[Int]("status")
        /** Database column commonName SqlType(VARCHAR), Length(128,true), Default(None) */
        val commonname: Rep[Option[String]] = column[Option[String]]("commonName", O.Length(128,varying=true), O.Default(None))
        /** Database column description SqlType(VARCHAR), Length(255,true), Default(None) */
        val description: Rep[Option[String]] = column[Option[String]]("description", O.Length(255,varying=true), O.Default(None))
    }
    /** Collection-like TableQuery object for table Transactions */
    lazy val Transactions = new TableQuery(tag => new Transactions(tag))

}
