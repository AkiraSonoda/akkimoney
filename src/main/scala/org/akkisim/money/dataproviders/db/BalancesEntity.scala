/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.dataproviders.db

/**
  * Entity class storing rows of table Balances
  *
  * @param avatarUUID    Database column avatarUUID SqlType(VARCHAR), PrimaryKey, Length(128,true)
  * @param balance Database column balance SqlType(INT)
  * @param status  Database column status SqlType(TINYINT), Default(None)
  * @author Akira Sonoda
  */
final case class BalancesEntity(avatarUUID: String, balance: Int, status: Int = 0)

final case class BalancesEntityUpdate(avatarUUID: String, balance: Int, status: Int = 0) {
    def merge(balances: BalancesEntity): BalancesEntity = {
        BalancesEntity(avatarUUID, balance, status)
    }
}