/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.utils

import org.akkisim.money.Configuration
import org.akkisim.money.services.FlywayService
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.slf4j.{Logger, LoggerFactory}


/**
  * @author Akira Sonoda
  */
class DbMigrationReloadSchemaSpec extends WordSpecLike with Matchers with BeforeAndAfterAll
    with Configuration  {
    val log: Logger = LoggerFactory.getLogger("DbMigrationReloadSchemaTest")

    val flywayService = new FlywayService(dbJDBCUrl, dbUsername, dbPassword)

    "The Schema Reload" should {
        "be performed correctly and produce a correct Return Code" in {
            // assert(true)
            val migrationResult = flywayService.reloadSchema()
        }
    }
}
