/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.http

import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import org.akkisim.money.dataproviders.helper.ProcessingResult
import org.scalatest.{BeforeAndAfter, BeforeAndAfterEach}
import org.scalatest.concurrent.ScalaFutures
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

class TransactionsRouteSpec extends BaseRouteTest with BeforeAndAfter with BeforeAndAfterEach with ScalaFutures {
    import StatusCodes._
    import io.circe.generic.auto._
    import io.circe.syntax._

    val Port = 9000
    val Host = "localhost"
    val wireMockServer = new WireMockServer(wireMockConfig().port(Port))

    trait Context {
        val transactionsRoute: Route = httpService.transactionsRouter.route
    }

    override def beforeEach {
        wireMockServer.start()
        WireMock.configureFor(Host, Port)
    }

    override def afterEach {
        wireMockServer.stop()
    }

    before {
        // Create the Sender
        val userinfoFuture66 = userinfoService.createUserInfo(user66)
        Await.result(userinfoFuture66, 10.seconds)
        // Create the Senders Balance
        val balancesFuture66 = balancesService.createBalance(balance66)
        Await.result(balancesFuture66, 10.seconds)

        // Create the Receiver
        val userinfoFuture67 = userinfoService.createUserInfo(user67)
        Await.result(userinfoFuture67, 10.seconds)

        val balancesFuture67 = balancesService.createBalance(balance67)
        Await.result(balancesFuture67, 10.seconds)
    }


    after {
        // Delete the Senders Balance
        val balancesFuture66 = balancesService.deleteBalance(balance66.avatarUUID,tokenHeader66.value)
        Await.result(balancesFuture66, 10.seconds)
        // Delete the Sender
        val userinfoFuture66 = userinfoService.deleteUserinfo(user66.avatarUUID)
        Await.result(userinfoFuture66, 10.seconds)


        val balancesFuture67 = balancesService.deleteBalance(balance67.avatarUUID,tokenHeader67.value)
        Await.result(balancesFuture67, 10.seconds)

        val transactionsFuture66 = transactionsService.deleteTransaction(tx66to67.uuid)
        Await.result(transactionsFuture66, 10.seconds)

        // Delete the Receiver
        val userinfoFuture67 = userinfoService.deleteUserinfo(user67.avatarUUID)
        Await.result(userinfoFuture67, 10.seconds)

    }

    "Transactions Service" should {
        "Process a transaction" in new Context {

            val path = "/"
            stubFor(post(urlEqualTo(path))
                .willReturn(
                    aResponse()
                        .withBody("<?xml version=\"1.0\" encoding=\"utf-8\"?><methodResponse><params><param><value><struct><member><name>success</name><value><boolean>1</boolean></value></member><member><name>clientBalance</name><value><i4>99550</i4></value></member></struct></value></param></params></methodResponse>")
                        .withStatus(200)))

            val requestEntity = HttpEntity(MediaTypes.`application/json`, tx66to67.asJson.toString())
            Post("/transactions", requestEntity).withHeaders(tokenHeader66) ~> transactionsRoute ~> check {
                responseAs[ProcessingResult].success should be(true)
            }
        }
//        "delete transaction" in new Context {
//            Delete("/transactions/102dba0-4711-11e7-9598-0800200c9b66") ~> transactionsRoute ~> check {
//                response.status should be(NoContent)
//                whenReady(transactionsService.getTransactionsByUUID("102dba0-4711-11e7-9598-0800200c9b69")) {
//                    result => result should be(None: Option[TransactionsEntity])
//                }
//            }
//        }
    }

}
