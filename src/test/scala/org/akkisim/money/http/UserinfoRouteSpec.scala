/*
 *  Copyright (c) Akira Sonoda 2016.
 *  
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.http

import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import org.akkisim.money.dataproviders.db.{UserinfoEntity, UserinfoEntityUpdate}
import org.akkisim.money.dataproviders.helper.Token
import org.scalatest.BeforeAndAfter
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class UserinfoRouteSpec extends BaseRouteTest with BeforeAndAfter with ScalaFutures {
    import StatusCodes._
    import userinfoService._
    import io.circe.generic.auto._
    import io.circe.syntax._

    trait Context {
        val userinfoRoute: Route = httpService.userinfoRouter.route
    }

    before {
        // Create the Sender
        val userinfoFuture66 = userinfoService.createUserInfo(user66)
        Await.result(userinfoFuture66, 10.seconds)

        // Create the Receiver
        val userinfoFuture67 = userinfoService.createUserInfo(user67)
        Await.result(userinfoFuture67, 10.seconds)

    }


    after {
        // Delete the Sender
        val userinfoFuture66 = userinfoService.deleteUserinfo(user66.avatarUUID)
        Await.result(userinfoFuture66, 10.seconds)

        // Delete the Receiver
        val userinfoFuture67 = userinfoService.deleteUserinfo(user67.avatarUUID)
        Await.result(userinfoFuture67, 10.seconds)

    }

    "Userinfo Service" should {
        "retrieve userinfo list" in new Context {
            Get("/userinfo") ~> userinfoRoute ~> check {
                responseAs[Seq[UserinfoEntity]].size should be(2)
            }
        }
        "retrieve userinfo by id" in new Context {
            Get("/userinfo/102dba0-4711-11e7-9598-0800200c9b66") ~> userinfoRoute ~> check {
                responseAs[UserinfoEntity].avatarUUID should be(user66.avatarUUID)
            }
        }
        "update userinfo by id and retrieve it" in new Context {
            val userinfoUpdate = UserinfoEntityUpdate(
                avatarUUID = user66.avatarUUID,
                avatarName = user66.avatarName,
                simAddress = user66.simAddress,
                sessionId = user66.sessionId,
                secureSessionId = user66.secureSessionId,
                password = Some("newPassword"),
                token = user66.token)

            val requestEntity = HttpEntity(MediaTypes.`application/json`, userinfoUpdate.asJson.toString())
            Put("/userinfo/102dba0-4711-11e7-9598-0800200c9b66", requestEntity) ~> userinfoRoute ~> check {
                responseAs[UserinfoEntity].avatarUUID should be(user66.avatarUUID)
                whenReady(getUserinfoByUser("102dba0-4711-11e7-9598-0800200c9b66")) { result =>
                    result.get.password should be(Some("newPassword"))
                }
            }
        }
        "create a new userinfo" in new Context {
            val userinfoCreate = UserinfoEntity(
                avatarUUID = "102dba0-4711-11e7-9598-0800200c9b69",
                avatarName = "Zaki Test3",
                simAddress = "127.0.0.1",
                password = None,
                sessionId = Some("5027b2fa-a0bb-47a5-ba27-c66279fc1af3"),
                secureSessionId = Some("f49eb4a1-fd9f-4abd-8338-69895b6ccdf4"),
                token = None)

            val requestEntity = HttpEntity(MediaTypes.`application/json`, userinfoCreate.asJson.toString())
            Post("/userinfo", requestEntity) ~> userinfoRoute ~> check {
                responseAs[Int] should be(1)
            }
        }
        "delete userinfo" in new Context {
            Delete("/userinfo/102dba0-4711-11e7-9598-0800200c9b69") ~> userinfoRoute ~> check {
                response.status should be(NoContent)
                whenReady(getUserinfoByUser("102dba0-4711-11e7-9598-0800200c9b69")) {
                    result => result should be(None: Option[UserinfoEntity])
                }
            }
        }
        "throw a BadRequest Message on missing security Elements" in new Context {
            val userinfoCreate = UserinfoEntity(
                avatarUUID = "102dba0-4711-11e7-9598-0800200c9b69",
                avatarName = "Zaki Test3",
                simAddress = "127.0.0.1",
                password = None,
                sessionId = None,
                secureSessionId = None,
                token = None)
            val requestEntity = HttpEntity(MediaTypes.`application/json`, userinfoCreate.asJson.toString())
            Post("/userinfo", requestEntity) ~> userinfoRoute ~> check {
                response.status should be(BadRequest)
            }
        }
    }

}
