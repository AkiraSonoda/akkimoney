package org.akkisim.money.http

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.testkit.ScalatestRouteTest
import scala.collection.immutable.Seq
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.akkisim.money.MoneyServer.databaseService
import org.akkisim.money.dataproviders.db._
import org.akkisim.money.services._
import org.scalatest.{Matchers, WordSpec}

trait BaseRouteTest extends WordSpec with Matchers with FailFastCirceSupport with ScalatestRouteTest {

    val databaseService = new DatabaseService()
    val userinfoService = new UserinfoService(databaseService)
    val authenticationService = new AuthenticationService(databaseService, userinfoService)
    val balancesService = new BalancesService(databaseService, authenticationService )
    val transactionsService = new TransactionsService(databaseService,userinfoService,balancesService, authenticationService)

    val httpService = new HttpService(
        balancesService,
        transactionsService,
        userinfoService
    )

    val tokenHeader68 = RawHeader("token", "102dba0-4711-11e7-9598-0800200c9b68")
    val tokenHeader67 = RawHeader("token", "102dba0-4711-11e7-9598-0800200c9b67")
    val tokenHeader66 = RawHeader("token", "102dba0-4711-11e7-9598-0800200c9b66")

    val avatarUUIDHeader68 = RawHeader("avatarUUID", "102dba0-4711-11e7-9598-0800200c9b68")
    val avatarUUIDHeader67 = RawHeader("avatarUUID", "102dba0-4711-11e7-9598-0800200c9b67")
    val avatarUUIDHeader66 = RawHeader("avatarUUID", "102dba0-4711-11e7-9598-0800200c9b66")

    val headers68: Seq[HttpHeader] = Seq(tokenHeader66, avatarUUIDHeader68)
    val headers67: Seq[HttpHeader] = Seq(tokenHeader66, avatarUUIDHeader67)
    val headers66: Seq[HttpHeader] = Seq(tokenHeader66, avatarUUIDHeader66)

    val balance66 = BalancesEntity(avatarUUID = "102dba0-4711-11e7-9598-0800200c9b66", balance = 2700, status = 0)
    val balance67 = BalancesEntity(avatarUUID = "102dba0-4711-11e7-9598-0800200c9b67", balance = 4300, status = 0)
    val balance68 = BalancesEntity(avatarUUID = "102dba0-4711-11e7-9598-0800200c9b68", balance = 1209, status = 0)

    val user66 = UserinfoEntity(
            avatarUUID = "102dba0-4711-11e7-9598-0800200c9b66",
            avatarName = "Akira Sonoda",
            simAddress = "http://localhost:9000",
            sessionId = Some("447d9fde-529b-40d3-b03f-9c08784256ff"),
            secureSessionId = Some("bd6be2e5-f524-417a-a99c-460f6dce73db"),
            password = Some("password"),
            token = Some("102dba0-4711-11e7-9598-0800200c9b66"))

    val user67 = UserinfoEntity(
            avatarUUID = "102dba0-4711-11e7-9598-0800200c9b67",
            avatarName = "Zaki Test1",
            simAddress = "http://localhost:9000",
            sessionId = Some("447d9fde-529b-40d3-b03f-9c08784256fa"),
            secureSessionId = Some("bd6be2e5-f524-417a-a99c-460f6dce73dc"),
            password = Some("password"),
            token = Some("102dba0-4711-11e7-9598-0800200c9b67"))

    val user68 = UserinfoEntity(
            avatarUUID = "102dba0-4711-11e7-9598-0800200c9b68",
            avatarName = "Zaki Test2",
            simAddress = "http://localhost:9000",
            sessionId = Some("447d9fde-529b-40d3-b03f-9c08784256fb"),
            secureSessionId = Some("bd6be2e5-f524-417a-a99c-460f6dce73dd"),
            password = Some("password"),
            token = Some("102dba0-4711-11e7-9598-0800200c9b68"))



    val tx66to67 =  TransactionsEntity(
            uuid = "102dba0-4711-11e7-9598-0800200c9b66",
            sender = "102dba0-4711-11e7-9598-0800200c9b66", // User66
            receiver = "102dba0-4711-11e7-9598-0800200c9b67", // User67
            amount = 100,
            objectuuid = None,
            regionhandle = "RegionA",
            `type` = 5008,
            time = 100000,
            secure = "SecureA",
            status = 1,
            commonname = None,
            description = Some("DescriptionA")
    )
    val tx67to68 = TransactionsEntity(
            uuid = "102dba0-4711-11e7-9598-0800200c9b67",
            sender = "102dba0-4711-11e7-9598-0800200c9b67",
            receiver = "102dba0-4711-11e7-9598-0800200c9b68",
            amount = 200,
            objectuuid = None,
            regionhandle = "RegionB",
            `type` = 5008,
            time = 100001,
            secure = "SecureB",
            status = 1,
            commonname = None,
            description = Some("DescriptionB")
    )
    val tx68to66 = TransactionsEntity(
            uuid = "102dba0-4711-11e7-9598-0800200c9b68",
            sender = "102dba0-4711-11e7-9598-0800200c9b68",
            receiver = "102dba0-4711-11e7-9598-0800200c9b66",
            amount = 300,
            objectuuid = None,
            regionhandle = "RegionC",
            `type` = 5008,
            time = 100002,
            secure = "SecureC",
            status = 1,
            commonname = None,
            description = Some("DescriptionC")
    )
}
