package org.akkisim.money.http

import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import org.akkisim.money.dataproviders.db.{BalancesEntity, BalancesEntityUpdate}
import org.scalatest.BeforeAndAfter
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

class BalancesRouteSpec extends BaseRouteTest with BeforeAndAfter with ScalaFutures {
    import StatusCodes._
    import balancesService._
    import io.circe.generic.auto._
    import io.circe.syntax._

    trait Context {
        val balancesRoute: Route = httpService.balancesRouter.route
    }

    before {
        // Create the Sender
        val userinfoFuture66 = userinfoService.createUserInfo(user66)
        Await.result(userinfoFuture66, 10.seconds)
        // Create the Senders Balance
        val balancesFuture66 = balancesService.createBalance(balance66)
        Await.result(balancesFuture66, 10.seconds)

        // Create the Receiver
        val userinfoFuture67 = userinfoService.createUserInfo(user67)
        Await.result(userinfoFuture67, 10.seconds)

        val balancesFuture67 = balancesService.createBalance(balance67)
        Await.result(balancesFuture67, 10.seconds)

        // Create a User without Balance
        val userinfoFuture68 = userinfoService.createUserInfo(user68)
        Await.result(userinfoFuture68, 10.seconds)
    }


    after {
        // Delete the Senders Balance
        val balancesFuture66 = balancesService.deleteBalance(balance66.avatarUUID,tokenHeader66.value)
        Await.result(balancesFuture66, 10.seconds)
        // Delete the Sender
        val userinfoFuture66 = userinfoService.deleteUserinfo(user66.avatarUUID)
        Await.result(userinfoFuture66, 10.seconds)

        val balancesFuture67 = balancesService.deleteBalance(balance67.avatarUUID,tokenHeader67.value)
        Await.result(balancesFuture67, 10.seconds)
        // Delete the Receiver
        val userinfoFuture67 = userinfoService.deleteUserinfo(user67.avatarUUID)
        Await.result(userinfoFuture67, 10.seconds)

        // Delete a User without Balance
        val userinfoFuture68 = userinfoService.deleteUserinfo(user68.avatarUUID)
        Await.result(userinfoFuture68, 10.seconds)

    }

    "Balances Service" should {
        "retrieve balances list" in new Context {
            Get("/balances").withHeaders(headers66) ~> balancesRoute ~> check {
                responseAs[Seq[BalancesEntity]].size should be(2)
            }
        }
        "retrieve balances by id" in new Context {
            Get("/balances/102dba0-4711-11e7-9598-0800200c9b66").withHeaders(tokenHeader66) ~> balancesRoute ~> check {
                responseAs[BalancesEntity].avatarUUID should be(user66.avatarUUID)
            }
        }
        "update balances by id and retrieve it" in new Context {
            val balancesUpdate = BalancesEntityUpdate(avatarUUID = "102dba0-4711-11e7-9598-0800200c9b66", balance = 200, status=99)

            val requestEntity = HttpEntity(MediaTypes.`application/json`, balancesUpdate.asJson.toString())
            Put("/balances/102dba0-4711-11e7-9598-0800200c9b66", requestEntity).withHeaders(tokenHeader66) ~> balancesRoute ~> check {
                responseAs[BalancesEntity].avatarUUID should be(user66.avatarUUID)
            }
            Get("/balances/102dba0-4711-11e7-9598-0800200c9b66").withHeaders(tokenHeader66) ~> balancesRoute ~> check {
                responseAs[BalancesEntity].status should be(99)
            }
        }
        "create a new balance" in new Context {
            val balanceCreate = BalancesEntity(avatarUUID = "102dba0-4711-11e7-9598-0800200c9b68", balance = 4711, status=99)
            val requestEntity = HttpEntity(MediaTypes.`application/json`, balanceCreate.asJson.toString())
            Post("/balances", requestEntity).withHeaders(tokenHeader68) ~> balancesRoute ~> check {
                responseAs[Int] should be(1)
            }
        }
        "delete balance" in new Context {
            Delete("/balances/102dba0-4711-11e7-9598-0800200c9b68").withHeaders(tokenHeader68) ~> balancesRoute ~> check {
                response.status should be(NoContent)
                whenReady(getBalancesByUser("102dba0-4711-11e7-9598-0800200c9b68")) {
                    result => result should be(None: Option[BalancesEntity])
                }
            }
        }
    }

}
