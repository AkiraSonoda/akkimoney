/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.services

import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.RawHeader
import akka.stream.ActorMaterializer
import org.akkisim.money.dataproviders.db._
import org.akkisim.money.http.HttpService

trait BaseServiceTest {
    implicit val system = ActorSystem("moneyserver")
    implicit val materializer = ActorMaterializer()

    import scala.concurrent.ExecutionContext.Implicits.global

    val databaseService = new DatabaseService()
    val userinfoService = new UserinfoService(databaseService)
    val authenticationService = new AuthenticationService(databaseService, userinfoService)
    val balancesService = new BalancesService(databaseService, authenticationService)
    val transactionsService = new TransactionsService(databaseService,userinfoService,balancesService, authenticationService)

    val httpService = new HttpService(
        balancesService,
        transactionsService,
        userinfoService
    )

    val header68 = RawHeader("token", "102dba0-4711-11e7-9598-0800200c9c68")
    val header67 = RawHeader("token", "102dba0-4711-11e7-9598-0800200c9c67")
    val header66 = RawHeader("token", "102dba0-4711-11e7-9598-0800200c9c66")

    val balanceUpdate66 = BalancesEntityUpdate(avatarUUID = "102dba0-4711-11e7-9598-0800200c9c66", balance = 2700, status = 0)
    val balanceUpdate67 = BalancesEntityUpdate(avatarUUID = "102dba0-4711-11e7-9598-0800200c9c67", balance = 4300, status = 0)
    val balanceUpdate68 = BalancesEntityUpdate(avatarUUID = "102dba0-4711-11e7-9598-0800200c9c68", balance = 1209, status = 0)

    val balance66 = BalancesEntity(avatarUUID = "102dba0-4711-11e7-9598-0800200c9c66", balance = 2700, status = 0)
    val balance67 = BalancesEntity(avatarUUID = "102dba0-4711-11e7-9598-0800200c9c67", balance = 4300, status = 0)
    val balance68 = BalancesEntity(avatarUUID = "102dba0-4711-11e7-9598-0800200c9c68", balance = 1209, status = 0)

    val user66 = UserinfoEntity(
            avatarUUID = "102dba0-4711-11e7-9598-0800200c9c66",
            avatarName = "Akira Sonoda",
            simAddress = "http://localhost:9000",
            sessionId = Some("447d9fde-529b-40d3-b03f-9c08784256ff"),
            secureSessionId = Some("bd6be2e5-f524-417a-a99c-460f6dce73db"),
            password = Some("password"),
            token = Some("102dba0-4711-11e7-9598-0800200c9c66"))

    val user67 = UserinfoEntity(
            avatarUUID = "102dba0-4711-11e7-9598-0800200c9c67",
            avatarName = "Zaki Test1",
            simAddress = "http://localhost:9000",
            sessionId = Some("447d9fde-529b-40d3-b03f-9c08784256fa"),
            secureSessionId = Some("bd6be2e5-f524-417a-a99c-460f6dce73dc"),
            password = Some("password"),
            token = Some("102dba0-4711-11e7-9598-0800200c9c67"))

    val user68 = UserinfoEntity(
            avatarUUID = "102dba0-4711-11e7-9598-0800200c9c68",
            avatarName = "Zaki Test2",
            simAddress = "http://localhost:9000",
            sessionId = Some("447d9fde-529b-40d3-b03f-9c08784256fb"),
            secureSessionId = Some("bd6be2e5-f524-417a-a99c-460f6dce73dd"),
            password = Some("password"),
            token = Some("102dba0-4711-11e7-9598-0800200c9c68"))



    val tx66to67 =  TransactionsEntity(
            uuid = "102dba0-4711-11e7-9598-0800200c9c66",
            sender = "102dba0-4711-11e7-9598-0800200c9c66", // User66
            receiver = "102dba0-4711-11e7-9598-0800200c9c67", // User67
            amount = 100,
            objectuuid = None,
            regionhandle = "RegionA",
            `type` = 5008,
            time = 100000,
            secure = "SecureA",
            status = 1,
            commonname = None,
            description = Some("DescriptionA")
    )
    val tx67to68 = TransactionsEntity(
            uuid = "102dba0-4711-11e7-9598-0800200c9c67",
            sender = "102dba0-4711-11e7-9598-0800200c9c67",
            receiver = "102dba0-4711-11e7-9598-0800200c9c68",
            amount = 200,
            objectuuid = None,
            regionhandle = "RegionB",
            `type` = 5008,
            time = 100001,
            secure = "SecureB",
            status = 1,
            commonname = None,
            description = Some("DescriptionB")
    )
    val tx68to66 = TransactionsEntity(
            uuid = "102dba0-4711-11e7-9598-0800200c9c68",
            sender = "102dba0-4711-11e7-9598-0800200c9c68",
            receiver = "102dba0-4711-11e7-9598-0800200c9c66",
            amount = 300,
            objectuuid = None,
            regionhandle = "RegionC",
            `type` = 5008,
            time = 100002,
            secure = "SecureC",
            status = 1,
            commonname = None,
            description = Some("DescriptionC")
    )
}
