/*
 *  Copyright (c) Akira Sonoda 2016.
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.services

import akka.actor.ActorSystem
import akka.util.Timeout
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.{aResponse, post, stubFor, urlEqualTo}
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import org.akkisim.money.dataproviders.helper.ProcessingResult
import org.scalatest.{BeforeAndAfter, BeforeAndAfterEach, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * @author Akira Sonoda
  */
class TransactionsServiceSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfter with BeforeAndAfterEach with BaseServiceTest {
    val log: Logger = LoggerFactory.getLogger("BalancesServiceSpec")

    val Port = 9000
    val Host = "localhost"
    val wireMockServer = new WireMockServer(wireMockConfig().port(Port))

    override def beforeEach {
        wireMockServer.start()
        WireMock.configureFor(Host, Port)
        val path = "/"
        stubFor(post(urlEqualTo(path))
            .willReturn(
                aResponse()
                    .withBody("<?xml version=\"1.0\" encoding=\"utf-8\"?><methodResponse><params><param><value><struct><member><name>success</name><value><boolean>1</boolean></value></member><member><name>clientBalance</name><value><i4>99550</i4></value></member></struct></value></param></params></methodResponse>")
                    .withStatus(200)))

    }

    override def afterEach {
        wireMockServer.stop()
    }


    after {
        // Delete the Senders Balance
        val balancesFuture66 = balancesService.deleteBalance(balance66.avatarUUID,header66.value)
        Await.result(balancesFuture66, Duration.Inf)
        // Delete the Sender
        val userinfoFuture66 = userinfoService.deleteUserinfo(user66.avatarUUID)
        Await.result(userinfoFuture66, Duration.Inf)


        val balancesFuture67 = balancesService.deleteBalance(balance67.avatarUUID,header67.value)
        Await.result(balancesFuture67, Duration.Inf)

        val transactionsFuture66 = transactionsService.deleteTransaction(tx66to67.uuid)
        Await.result(transactionsFuture66, Duration.Inf)

        // Delete the Receiver
        val userinfoFuture67 = userinfoService.deleteUserinfo(user67.avatarUUID)
        Await.result(userinfoFuture67, Duration.Inf)
    }


    info("As TransactionsService User")
    info("I want to be able to create, read, update, delete Transactions")
    info("in order to have a quick overview of each grid users transactions")

    feature("A TransactionsService to manage transactions") {

        scenario("insert a transaction") {

            Given("a sender is available")
            val user66Future = userinfoService.createUserInfo(user66)
            val user66Result: Try[Int] = Await.ready(user66Future, Duration.Inf).value.get
            And("the sender has a balance")
            val balance66Future = balancesService.createBalance(balance66)
            val balance66Result: Try[Int] = Await.ready(balance66Future, Duration.Inf).value.get
            And("A receiver is available")
            val user67Future = userinfoService.createUserInfo(user67)
            val user67Result: Try[Int] = Await.ready(user67Future, Duration.Inf).value.get
            And("The receiver has a balance")
            val balance67Future = balancesService.createBalance(balance67)
            val balance67Result: Try[Int] = Await.ready(balance67Future, Duration.Inf).value.get
            When("a transcaction is created with the TransactionsService")
            val transactionsEntityFuture = transactionsService.createTransaction(tx66to67,header66.value)
            Then("the result should be the number of created TransactionsEntities")
            val result: Try[ProcessingResult] = Await.ready(transactionsEntityFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t.success == true)
                case Failure(e) => fail("Failure in createTransaction:", e)
            }
        }

//        scenario("Read all transactions") {
//            Given("Some transactions are inserted")
//            val transactionsFuture = transactionsService.getTransactions
//            When("I read all transactions")
//            Then("I should get a List of transactions")
//            val result: Try[Seq[TransactionsEntity]] = Await.ready(transactionsFuture, Duration.Inf).value.get
//            result match {
//                case Success(resultset) =>
//                    assert(resultset.size == 1)
//                    resultset.foreach(a => {
//                        assert(a.sender.equalsIgnoreCase("akira"))
//                        assert(a.amount == 42)
//                    })
//                case Failure(e) => fail("failure in getTransactions:", e)
//            }
//
//        }
//
//        scenario("Read specific transactions") {
//            Given("Some transactions are inserted")
//            When("I read the transaction with the uuid \"2102dba0-4bc7-11e7-9598-0800200c9a66\"")
//            val transactionsFuture = transactionsService.getTransactionsByUUID("2102dba0-4bc7-11e7-9598-0800200c9a66")
//            Then("I should get one balance with the value uuid \"2102dba0-4bc7-11e7-9598-0800200c9a66\" and an amount of 42")
//            val result: Try[Option[TransactionsEntity]] = Await.ready(transactionsFuture, Duration.Inf).value.get
//            result match {
//                case Success(transactions) =>
//                    transactions match {
//                        case Some(tx) =>
//                            assert(tx.sender.equalsIgnoreCase("akira"))
//                            assert(tx.amount == 42)
//                        case None => fail("No Transaction found for uuid \"2102dba0-4bc7-11e7-9598-0800200c9a66\"")
//                    }
//                case Failure(e) => log.error("Failure in getBalanceByUser ", e)
//            }
//
//        }

    }
}


