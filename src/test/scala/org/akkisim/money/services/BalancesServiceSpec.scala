/*
 * Copyright (c) Akira Sonoda 2016.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License (AGPL)
 * as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  See License.txt
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.services

import org.akkisim.money.dataproviders.db.{BalancesEntity, BalancesEntityUpdate, UserinfoEntity, UserinfoEntityUpdate}
import org.akkisim.money.dataproviders.helper.Token
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * @author Akira Sonoda
  */
class BalancesServiceSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfter with BaseServiceTest {
    val log: Logger = LoggerFactory.getLogger("BalancesServiceSpec")

    before {
        // Create the a User
        val userinfoFuture66 = userinfoService.createUserInfo(user66)
        Await.result(userinfoFuture66, 10.seconds)

        // Create the Receiver
        val userinfoFuture67 = userinfoService.createUserInfo(user67)
        Await.result(userinfoFuture67, 10.seconds)
        // Create Balance
        val balancesFuture67 = balancesService.createBalance(balance67)
        Await.result(balancesFuture67, 10.seconds)

    }


    after {
        // Delete the Senders Balance
        val balancesFuture66 = balancesService.deleteBalance(balance66.avatarUUID,header66.value)
        Await.result(balancesFuture66, 10.seconds)
        // Delete the Sender
        val userinfoFuture66 = userinfoService.deleteUserinfo(user66.avatarUUID)
        Await.result(userinfoFuture66, 10.seconds)

        val balancesFuture67 = balancesService.deleteBalance(balance67.avatarUUID,header67.value)
        Await.result(balancesFuture67, 10.seconds)
        // Delete the Receiver
        val userinfoFuture67 = userinfoService.deleteUserinfo(user67.avatarUUID)
        Await.result(userinfoFuture67, 10.seconds)

    }


    info("As BalancesService User")
    info("I want to be able to create, read, update, delete Balances")
    info("in order to have a quick overview of each grid users balances")

    var token: String = ""


    feature("A BalancesService to manage balances") {

        scenario("01_insert a balance") {

            Given("a avatarUUID is created")
            When("a balance is created with the balancesService")
            val balancesEntityFuture = balancesService.createUpdateBalance(balanceUpdate66.avatarUUID,header66.value,balanceUpdate66)

            Then("the result should be the number of created BalanceEntities")
            val result: Try[Option[BalancesEntity]] = Await.ready(balancesEntityFuture, Duration.Inf).value.get
            result match {
                case Success(optBalance) =>
                    optBalance match {
                        case Some(balance) => assert(balance.balance == 2700)
                        case None => fail("No Balance returned")
                    }
                case Failure(e) => fail("Failure in createBalance:", e)
            }
        }

        scenario("02_Read all balances") {
            Given("Some balances are inserted")
            When("I read all balances")
            val balancesFuture = balancesService.getBalances(user66.avatarUUID,header66.value)
            Then("I should get a List of balances")
            val result: Try[Seq[BalancesEntity]] = Await.ready(balancesFuture, Duration.Inf).value.get
            result match {
                case Success(resultset) =>
                    assert(resultset.size == 1)
                    resultset.foreach(a => {
                        assert(a.status == 0)
                    })
                case Failure(e) => fail("failure in getBalances:", e)
            }
        }

        scenario("03_Read specific balances") {
            Given("Some balances are inserted")
            When("I read the balance of avatarUUID \""+user67.avatarUUID+"\"")
            val balancesFuture = balancesService.getBalancesByUser(user67.avatarUUID, header67.value)
            Then("I should get one balance with the value avatarUUID akira and a value of 100")
            val result: Try[Option[BalancesEntity]] = Await.ready(balancesFuture, Duration.Inf).value.get
            result match {
                case Success(balances) =>
                    balances match {
                        case Some(balance) =>
                            assert(balance.avatarUUID.equalsIgnoreCase(user67.avatarUUID))
                            assert(balance.balance == balanceUpdate67.balance)
                        case None => fail("No balance found for avatarUUID \""+user67.avatarUUID+"\"")
                    }
                case Failure(e) => log.error("Failure in getBalanceByUser ", e)
            }
        }

        scenario("04_Update a specific balance") {
            Given("Some balances are inserted")
            When("I update the balance of avatarUUID \""+user67.avatarUUID+"\" to 200")
            val balanceUpdate = BalancesEntityUpdate(avatarUUID = user67.avatarUUID, balance = 200)
            val balancesFuture = balancesService.updateBalance(user67.avatarUUID,balanceUpdate,header67.value)
            Then("I should get one balance with the value avatarUUID "+user66.avatarUUID+" and a value of 200")
            val result: Try[Option[BalancesEntity]] = Await.ready(balancesFuture, Duration.Inf).value.get
            result match {
                case Success(balances) =>
                    balances match {
                        case Some(balance) =>
                            assert(balance.avatarUUID.equalsIgnoreCase(user67.avatarUUID))
                            assert(balance.balance == 200)
                        case None => fail("No balance found for avatarUUID \""+user67.avatarUUID+"\"")
                    }
                case Failure(e) => log.error("Failure in updateBalance ", e)
            }
        }

        scenario("05_delete a balance") {
            Given("a balance of the avatarUUID \""+user67.avatarUUID+"\" is inserted")
            When("I delete the balance of \""+user67.avatarUUID+"\"")
            val balancesFuture = balancesService.deleteBalance(user67.avatarUUID,header67.value)
            Then("should get the number of deleted balances")
            val result: Try[Int] = Await.ready(balancesFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in deleteBalance:", e)
            }
        }
    }
}


