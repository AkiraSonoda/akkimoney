/*
 *  Copyright (c) Akira Sonoda 2016.
 *  
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU Affero General Public License (AGPL)
 *  as published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  See License.txt
 *  If not, see <http://www.gnu.org/licenses/>.
 */

package org.akkisim.money.services

import org.akkisim.money.dataproviders.db.{UserinfoEntity, UserinfoEntityUpdate}
import org.akkisim.money.dataproviders.helper.Token
import org.scalatest.{FeatureSpec, GivenWhenThen}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
  * @author Akira Sonoda
  */
class UserinfoServiceSpec extends FeatureSpec with GivenWhenThen {
    val log: Logger = LoggerFactory.getLogger("BalancesServiceSpec")

    import scala.concurrent.ExecutionContext.Implicits.global
    val userinfoService = new UserinfoService(new DatabaseService())

    info("As UserinfoService User")
    info("I want to be able to create, read, update, delete Userinfo")
    info("in order to have a quick overview of each grid users Userinfo")

    feature("A UserinfoService to manage Userinfo") {

        scenario("insert a Userinfo") {
            Given("a Userinfo is available")
            val userInfo = UserinfoEntity(
                avatarUUID = "04926362-0d29-48e7-8965-ea2707d5a142",
                avatarName = "akira",
                simAddress = "localhost",
                sessionId = Some("fc18c10c-2e61-4f99-8232-4b0e420e6f91"),
                secureSessionId = Some("12b414f5-22af-475f-bf73-1090023b6cca"),
                password = Some("password"),
                token = None
            )
            When("a Userinfo is created with the UserinfoService")
            val userinfoFuture = userinfoService.createUserInfo(userInfo)
            Then("the result should be the number of created UserinfoEntities")
            val result: Try[Int] = Await.ready(userinfoFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in createUserinfo:", e)
            }

        }

        scenario("Read all Userinfo") {
            Given("Some Userinfo are inserted")
            When("I read all Userinfo")
            val userinfoFuture = userinfoService.getUserinfos
            Then("I should get a List of Userinfo")
            val result: Try[Seq[UserinfoEntity]] = Await.ready(userinfoFuture, Duration.Inf).value.get
            result match {
                case Success(resultset) =>
                    assert(resultset.size == 1)
                    resultset.foreach(a => {
                        assert(a.avatarName.equalsIgnoreCase("akira"))
                        a.password match {
                            case Some(p) => assert(p.equalsIgnoreCase("password"))
                            case None => fail("No password was passed back")
                        }
                    })
                case Failure(e) => fail("failure in getUserinfo:", e)
            }
        }

        scenario("Read specific Userinfo") {
            Given("Some Userinfo are inserted")
            When("I read the Userinfo with the avatarUUID \"04926362-0d29-48e7-8965-ea2707d5a142\"")
            val userinfoFuture = userinfoService.getUserinfoByUser("04926362-0d29-48e7-8965-ea2707d5a142")
            Then("I should get one balance with the avatarUUID \"akira\" and a passeword \"password\"")
            val result: Try[Option[UserinfoEntity]] = Await.ready(userinfoFuture, Duration.Inf).value.get
            result match {
                case Success(userinfo) =>
                    userinfo match {
                        case Some(ui) =>
                            assert(ui.avatarName.equalsIgnoreCase("akira"))
                            ui.password match {
                                case Some(p) => assert(p.equalsIgnoreCase("password"))
                                case None => fail("No password was passed back")
                            }
                        case None => fail("No userinfo found for avatarUUID \"akira\"")
                    }
                case Failure(e) => log.error("Failure in getUserinfoByUser ", e)
            }

        }

        scenario("Update a specific Userinfo") {
            Given("Some Userinfo are inserted")
            When("I update the Userinfo with avatarUUID \"akira\" to password \"secret\"")
            val userinfoUpdate = UserinfoEntityUpdate(
                avatarUUID = "04926362-0d29-48e7-8965-ea2707d5a142",
                avatarName = "akira",
                simAddress = "localhost",
                sessionId = Some("fc18c10c-2e61-4f99-8232-4b0e420e6f91"),
                secureSessionId = Some("12b414f5-22af-475f-bf73-1090023b6cca"),
                password = Some("secret"),
                token = None
            )
            val userinfoFuture = userinfoService.updateUserinfo(userinfoUpdate.avatarUUID, userinfoUpdate)
            Then("I should get one Userinfo with the avatarUUID \"akira\" and a password \"secret\"")
            val result: Try[Option[UserinfoEntity]] = Await.ready(userinfoFuture, Duration.Inf).value.get
            result match {
                case Success(userinfo) =>
                    userinfo match {
                        case Some(ui) =>
                            assert(ui.avatarName.equalsIgnoreCase("akira"))
                            ui.password match {
                                case Some(p) => assert(p.equalsIgnoreCase("secret"))
                                case None => fail("No password was passed back")
                            }
                        case None => fail("No userinfo found for avatarUUID \"akira\"")
                    }
                case Failure(e) => log.error("Failure in getUserinfoByUser ", e)
            }
        }

        scenario("delete a Userinfo") {
            Given("a Userinfo with avatarUUID \"akira\" is inserted")
            When("I delete the Userinfo with avatarUUID \"04926362-0d29-48e7-8965-ea2707d5a142\"")
            val userinfoFuture = userinfoService.deleteUserinfo("04926362-0d29-48e7-8965-ea2707d5a142")
            Then("should get the number of deleted Userinfo")
            val result: Try[Int] = Await.ready(userinfoFuture, Duration.Inf).value.get
            result match {
                case Success(t) => assert(t == 1)
                case Failure(e) => fail("Failure in deleteUserInfo:", e)
            }
        }

    }
}


