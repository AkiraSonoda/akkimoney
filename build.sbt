name := "akkimoney"
organization  := "org.akkigrid"
version := "1.0"
scalaVersion := "2.13.1"
scalacOptions := Seq("-unchecked", "-feature", "-deprecation", "-encoding", "utf8")

parallelExecution in Test := false
parallelExecution in IntegrationTest := false
testForkedParallel in Test := false
testForkedParallel in IntegrationTest := false

assemblyJarName in assembly := "akkimoney.jar"
test in assembly := {}


libraryDependencies ++= {
    val akkaHttpCoreV    = "10.1.11"
    val slickV           = "3.3.2"
    val circeV           = "0.13.0"
    val scalaTestV       = "3.1.1"

    Seq(
        "com.typesafe.akka"  %% "akka-http"                        % akkaHttpCoreV, // HTTP server
        "com.typesafe.akka"  %% "akka-http-core"                   % akkaHttpCoreV,
        "ch.megard"          %% "akka-http-cors"                   % "0.4.2",       // Support of CORS requests, version depends on akka-http

        "com.typesafe.slick" %% "slick"                            % slickV,        // SQL generator

        "mysql"              %  "mysql-connector-java"             % "5.1.42",      // MySql driver
        "org.flywaydb"       %  "flyway-core"                      % "4.2.0",       // Migration for SQL databases
        "com.zaxxer"         %  "HikariCP"                         % "2.7.0",       // Connection pool for database
	      "com.outr"           %% "hasher"                           % "1.2.2",       // Encoding decoding sugar, used in passwords hashing
	      "com.pauldijou"      %% "jwt-core"                         % "4.2.0",       // Parsing and generating of JWT tokens
        "com.github.pureconfig" %% "pureconfig"                    % "0.12.2",      // Config file parser

        "io.circe"           %% "circe-core"                       % circeV,        // JSON serialization library
        "io.circe"           %% "circe-generic"                    % circeV,
        "io.circe"           %% "circe-parser"                     % circeV,

        "de.heikoseeberger"  %% "akka-http-circe"                  % "1.31.0",      // Sugar for serialization and deserialization in akka-http with circ
		    "com.wix"            %% "accord-core"                      % "0.7.5",       // Validation library

        "org.scalatest"      %% "scalatest"                        % scalaTestV     % "test",
        "com.typesafe.akka"  %% "akka-http-testkit"                % akkaHttpCoreV  % "test",
        "com.typesafe.akka"  %% "akka-testkit"                     % "2.6.3"        % "test",
        "com.github.tomakehurst" % "wiremock"                      % "2.12.0"       % "test",
        "com.typesafe.akka"  %% "akka-slf4j"                       % "2.6.3",

        "org.slf4j"          %  "slf4j-api"                        % "1.7.25",      // Logging
        "ch.qos.logback"     % "logback-classic"                   % "1.2.3",
        "ch.qos.logback"     % "logback-core"                      % "1.2.3",
        "javax.xml.bind"     % "jaxb-api"                          % "2.3.1",

        "com.typesafe.slick" %% "slick-codegen"                    % slickV
    )
}

